#!/bin/bash
# update the package list and install necessary packages
apt-get update
apt-get install -y git openjdk-11-jdk


# install gitlab-runner
apt-get update
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash
apt-get install gitlab-runner
apt-cache madison gitlab-runner
apt-get install gitlab-runner=15.11.0



# install and setup docker (register for artifacts repo if necessary)
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
chmod 666 /var/run/docker.sock
curl -sSfL https://raw.githubusercontent.com/docker/scout-cli/main/install.sh | sh -s --
groupadd docker
usermod -aG docker $USER
# gcloud auth configure-docker us-central1-docker.pkg.dev


# install and config kubernetes
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
# gcloud container clusters get-credentials sit-cluster --zone us-central1-a --project final-project
# gcloud components install gke-gcloud-auth-plugin


# install and config checkov
apt-get install python3-pip -y
pip install checkov
ln -s /usr/local/bin/checkov ~/.local/bin/checkov
# pip install --upgrade attrs


# install and config terraform 
apt-get update && apt-get install -y gnupg software-properties-common
wget -O- https://apt.releases.hashicorp.com/gpg | \
gpg --dearmor | \
tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
tee /etc/apt/sources.list.d/hashicorp.list
apt-get update && apt-get install terraform
ln -s /usr/local/bin/terraform ~/.local/bin/terraform
# gcloud auth application-default login --project $PROJECT (login again if necessary)


# install Jenkins
sudo wget -O /etc/yum.repos.d/jenkins.repo \
    https://pkg.jenkins.io/redhat-stable/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
sudo yum upgrade
sudo yum install java-11-openjdk
sudo yum install jenkins
sudo systemctl daemon-reload

# install AWS CLI
apt-get install -y awscli
# configure AWS CLI
# aws configure set aws_access_key_id $ACCESS_KEY
# aws configure set aws_secret_access_key $SECRET_KEY
# aws configure set default.region $REGION


# install nodejs and angular
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
apt-get install -y nodejs
apt-get install -y npm
npm install ts-node -D
npm install -g react-native-cli


# install mongodb
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-5.0.list
apt-get update
apt-get install -y mongodb-org
# systemctl start mongod
## systemctl daemon-reload
