locals {
  global_config = {
    region = "us-east-1"
    # project = "minhph"
  }

  global_tags = {
    Environment    = "base"
    Owner          = "minhph"
    Project        = "final-project-minhph"
    Provisioned_by = "terraform"
  }

  vpc_config = {
    vpc_name   = "minhph-vpc"
    cidr_block = "10.0.0.0/16"
    subnets = {
      "public-subn-1" = {
        cidr_block              = "10.0.1.0/24"
        availability_zone       = "us-east-1b"
        map_public_ip_on_launch = true
        route_table             = "public-subn-rb"
      }
      "private-subn-1" = {
        cidr_block        = "10.0.2.0/24"
        availability_zone = "us-east-1b"
        route_table       = "private-subn-rb"
      }
    }
    internet_gateway = true
    nat_gateways = {
      "nat-gw-1" = {
        subnet = "public-subn-1"
      }
    }
    security_groups = {
      "us-1" = {
        name        = "us-rule-1"
        description = "Allow popular Inbound Outbound"
        ingress = [
          {
            from_port  = 0
            to_port    = 1024
            protocol   = "tcp"
            cidr_block = ["0.0.0.0/0"]
          }
        ],
        egress = [
          {
            from_port        = 0
            to_port          = 0
            protocol         = "-1"
            cidr_block       = ["0.0.0.0/0"]
            ipv6_cidr_blocks = ["::/0"]
          }
        ]
      }
    }
    route_tables = {
      "public-subn-rb" = {
        route = [
          {
            destination_cidr_block = "0.0.0.0/0"
            gateway                = "internet_gateway"
          }
        ]
      },
      "private-subn-rb" = {
        route = [
          {
            destination_cidr_block = "0.0.0.0/0"
            nat_gateway            = "nat-gw-1"
          }
        ]
      }
    }
  }


  ec2_config = {
    "public-ec2-1" = {
      ami             = "ami-080e1f13689e07408"
      instance_type   = "t2.medium"
      subnet          = "public-subn-1"
      security_groups = ["us-1"]
      userdata_file   = file("./files/user-data.sh")
      iam_role_name   = "ecr-admin"
      key_config = {
        key_name   = "minhph-ssh-key"
        public_key = file("./files/id_rsa.pub")
      }
    }
  }


  s3_config = {
    "${var.prefix}-code-pipeline-bucket" = {

    }
  }


  iam_role_config = {
    "ecr-admin" = {
      assume_role_policy   = file("./files/assume-role-policy-ecr-admin.json")
      identity_role_policy = file("./files/identity-role-policy-ecr-admin.json")
      description          = "This role enables access to ecr resources"
    }
  }
}