### What this repo contains:
- The base to run all related work

### What's in the base:
- The base includes:
    + A ec2 instance
    + A vpc module including:
        * Subnets for hosting ec2 instance and services
        * Security groups for controlling traffic
        * Other vpc components for networking like: nat gateway, route table, etc. 
