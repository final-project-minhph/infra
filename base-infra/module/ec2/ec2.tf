resource "aws_instance" "ec2_instance" {
  for_each             = var.ec2_config
  instance_type        = each.value.instance_type
  ami                  = each.value.ami
  subnet_id            = var.subnet[each.value.subnet].id
  security_groups      = toset([for sg_name in each.value.security_groups : var.sg[sg_name].id])
  key_name             = aws_key_pair.key_pair.key_name
  user_data            = each.value.userdata_file
  iam_instance_profile = aws_iam_instance_profile.instance_profile[each.key].name
  tags = merge({
    Name = each.key
  }, var.global_tags)
}


resource "aws_iam_instance_profile" "instance_profile" {
  for_each = var.ec2_config
  name     = "${each.key}-profile"
  role     = var.iam_role[each.value.iam_role_name].name
}


resource "aws_key_pair" "key_pair" {
  for_each   = var.ec2_config.key_config
  key_name   = each.value.key_name
  public_key = each.value.public_key
  tags       = var.global_config
}
