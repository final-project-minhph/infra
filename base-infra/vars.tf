variable "prefix" {
  description = "Prefix for resources names"
  default     = "minhph-final-proj"
}

variable "access_key" {
  description = "Access key of AWS resources"
  type        = string
}

variable "secret_key" {
  description = "Secret key of AWS resources"
  type        = string
}
