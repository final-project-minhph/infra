module "vpc" {
  source = "./module/vpc"

  global_config = local.global_config
  global_tags   = local.global_tags
  vpc_config    = local.vpc_config
}


module "iam_role" {
  source = "./module/iam"

  global_config   = local.global_config
  global_tags     = local.global_tags
  iam_role_config = local.iam_role_config
}


module "ec2" {
  source = "./module/ec2"

  global_config = local.global_config
  global_tags   = local.global_tags
  ec2_config    = local.ec2_config
  sg            = module.vpc.security_group
  subnet        = module.vpc.subnets
  iam_role      = module.iam_role.role

  depends_on = [module.iam_role]
}
