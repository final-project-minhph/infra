locals {
  global_variables = {
    environment = "sit"
    az          = "us-east-1"
    owner       = "minhph"
    project     = "final-project-minhph"
  }

  tags = {
    environment    = "sit"
    owner          = "minhph"
    name           = "minhph"
    project        = "final-project-minhph"
    provisioned_by = "terraform"
  }
}

