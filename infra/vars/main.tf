locals {
  env = {
    "sit" : local.sit,
    "uat" : local.uat,
    "prod" : local.prod,
  }

}

output "configuration" {
  value = local.env["${var.env}"]
}

output "global_variables" {
  value = local.global_variables
}

# output "configuration" {
#   value = local.var_file["${var.var_file}"]
# }

output "tags" {
  value = local.tags
}
