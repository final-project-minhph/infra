locals {
  sit = {
    vpc_config = {
      vpc_name   = "${var.env}-minhph-vpc"
      cidr_block = "10.0.0.0/16"
      subnets = {
        "${var.env}-public-subn-1" = {
          cidr_block              = "10.0.1.0/24"
          availability_zone       = "us-east-1a"
          map_public_ip_on_launch = true
          route_table             = "${var.env}-public-subn-rb"
        }
        "${var.env}-public-subn-2" = {
          cidr_block              = "10.0.2.0/24"
          availability_zone       = "us-east-1b"
          map_public_ip_on_launch = true
          route_table             = "${var.env}-public-subn-rb"
        }
        "${var.env}-private-subn-1" = {
          cidr_block        = "10.0.3.0/24"
          availability_zone = "us-east-1c"
          route_table       = "${var.env}-private-subn-rb"
        }
        "${var.env}-private-subn-2" = {
          cidr_block        = "10.0.4.0/24"
          availability_zone = "us-east-1d"
          route_table       = "${var.env}-private-subn-rb"
        }
      }
      internet_gateway = true
      nat_gateways = {
        "${var.env}-nat-gw-1" = {
          subnet = "${var.env}-public-subn-1"
        }
      }
      security_groups = {
        "${var.env}-us-1" = {
          # name        = "us-rule-1"
          description = "Allow popular In Out"
          ingress = [
            {
              from_port  = 0
              to_port    = 1024
              protocol   = "tcp"
              cidr_block = ["0.0.0.0/0"]
            }
          ],
          egress = [
            {
              from_port        = 0
              to_port          = 0
              protocol         = "-1"
              cidr_block       = ["0.0.0.0/0"]
              ipv6_cidr_blocks = ["::/0"]
            }
          ]
        }
      }
      route_tables = {
        "${var.env}-public-subn-rb" = {
          route = [
            {
              destination_cidr_block = "0.0.0.0/0"
              gateway                = "internet_gateway"
            }
          ]
        },
        "${var.env}-private-subn-rb" = {
          route = [
            {
              destination_cidr_block = "0.0.0.0/0"
              nat_gateway            = "${var.env}-nat-gw-1"
            }
          ]
        }
      }
    }


    alb_config = {
      name                       = "${var.env}-alb-1"
      load_balancer_type         = "application"
      security_groups            = ["${var.env}-us-1"]
      subnets                    = ["${var.env}-public-subn-1", "${var.env}-public-subn-2"]
      enable_deletion_protection = false
      lb_target_group = {
        name_prefix = "${var.env}-lb-tg"
        port        = 80
        protocol    = "HTTP"
        health_check = {
          interval            = 5
          healthy_threshold   = 3
          unhealthy_threshold = 3
          timeout             = 3
          path                = "/"
          port                = "80"
          matcher             = "200"
        }
      }

      lb_listener = [{
        port     = "80"
        protocol = "HTTP"
        default_action = {
          type             = "forward"
          target_group_arn = "asg"
        }
      }]
    }


    ##########################
    # AUTO SCALING GROUP CONFIG
    ##########################
    asg_config = {
      availability_zones = ["ap-southeast-2a"]
      desired_capacity   = 1
      max_size           = 3
      min_size           = 1
      launch_template_config = {
        name_prefix   = "webservers-"
        image_id      = "ami-0a709bebf4fa9246f"
        instance_type = "t2.micro"
      }
    }


    ecs_config = {
      name = "${var.env}-init-cluster"
    }


    rds_config = {
      name                 = "${var.env}-sql-instance"
      allocated_storage    = 1
      storage_type         = "gp2"
      engine               = "mysql"
      engine_version       = "8.0.35"
      instance_class       = "db.t2.small"
      username             = "admin"
      password             = "private!@#123"
      db_subnet_group_name = "${var.env}-db-subnet-group"
      subnet_names         = ["${var.env}-private-subn-1", "${var.env}-private-subn-2"]
    }


    ecr_config = {
      repository_name = "${var.env}-repository"
    }


    iam_role_config = {
      # "ecr-admin" = {
      #   assume_role_policy   = file("./help_files/assume-role-policy-ecr-admin.json")
      #   identity_role_policy = file("./help_files/identity-role-policy-ecr-admin.json")
      #   description          = "This role enables access to ecr resources"
      # }
      "code-pipeline-user" = {
        assume_role_policy = file("../vars/help_files/assume-role-policy-code-pipeline-user.json")
        identity_role_policy = templatefile("../vars/help_files/identity-role-policy-code-pipeline-user.json.tpl", {
          # haven't fixed this yet
          # CODEPIPELINE_BUCKET_ARN  = main.module.s3_bucket.arn,
          # CODESTAR_CONNECTIONS_ARN = main.module.code_pipeline.connections.arn
        })
        description = "This role enables access and supports management for codepipeline resources"
      }
      # "ecs-admin" = {
      #   role_policy = file("./help_files/ecs-admin.json")
      # }
    }


    s3_config = {
      "${var.env}-code-pipeline-bucket" = {

      }
    }


    code_pipeline_config = {
      name                  = "${var.env}-code-pipeline"
      iam_role              = "code-pipeline-user"
      artifact_store_bucket = "${var.env}-code-pipeline-bucket"
      stage = {
        "Source" = {
          action = {
            name             = "Source"
            category         = "Source"
            owner            = "ThirdParty"
            provider         = "GitLab"
            version          = "1"
            output_artifacts = ["source_output"]

            configuration = { #needs to be redefine
              Owner  = "minhunderscore"
              Repo   = "https://gitlab.com/final-project-minhph/sample-backend/"
              Branch = "main"
              # OAuthToken = "YOUR_GITHUB_TOKEN"
            }
          }
        }
        "Build" = {
          action = {
            name             = "Build"
            category         = "Build"
            owner            = "AWS"
            provider         = "CodeBuild"
            input_artifacts  = ["source_output"]
            version          = "1"
            output_artifacts = ["build_output"]

            configuration = {
              ProjectName = "${var.env}-code-build"
            }
          }
        }
        "Deloy" = {
          action = {
            name            = "Deploy"
            category        = "Deploy"
            owner           = "AWS"
            provider        = "CodeDeployToECS"
            input_artifacts = ["build_output"]
            version         = "1"

            configuration = { #needs to be redefine
              ClusterName     = "${var.env}-init-cluster"
              ServiceName     = "YOUR_ECS_SERVICE_NAME"
              FileName        = "imagedefinitions.json"
              AppSpecTemplate = "ecs-template.json"
              TaskDefinition  = "YOUR_TASK_DEFINITION"
              ECRRepository   = "${var.env}-repository"
            }
          }
        }
      }
      connection = {
        "${var.env}-gitlab-connection" = {
          type = "GitLab"
        }
      }
    }


    code_build_config = {
      name         = "${var.env}-code-build"
      description  = ""
      service_role = "code-pipeline-user"
      environment = {
        compute_type = "BUILD_GENERAL1_SMALL"
        image        = "aws/codebuild/amazonlinux2-x86_64-standard:4.0"
        type         = "LINUX_CONTAINER"
        # privileged_mode = ""
      }
      build_timeout = "60"
      source = {
        type = "S3"
        # full path to the s3 bucket
        location = "https://${local.global_variables.az}.console.aws.amazon.com/s3/buckets/${var.env}-code-pipeline-bucket}"
        # location = "${var.env}-code-pipeline-bucket"
      }
      artifacts = {
        type = "NO_ARTIFACTS"
      }
    }
  }


  uat = {

  }


  prod = {

  }
}
