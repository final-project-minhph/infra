resource "aws_db_instance" "db_instance" {
  db_name              = var.rds_config.name
  allocated_storage    = var.rds_config.allocated_storage
  storage_type         = var.rds_config.storage_type
  engine               = var.rds_config.engine
  engine_version       = var.rds_config.engine_version
  instance_class       = var.rds_config.instance_class
  username             = var.rds_config.username
  password             = var.rds_config.password
  db_subnet_group_name = aws_db_subnet_group.db_subnet_group.name
  tags                 = var.global_tags
}

resource "aws_db_subnet_group" "db_subnet_group" {
  name = var.rds_config.db_subnet_group_name
  subnet_ids = flatten([
    for subnet_name in var.rds_config.subnet_names : [
      for subnet in var.subnets : subnet.id if subnet.tags.Name == subnet_name
    ]
  ])
  tags = var.global_tags
}
