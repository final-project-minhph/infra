resource "aws_lb" "alb" {
  name               = var.alb_config.name
  internal           = false
  load_balancer_type = "application"
  subnets = flatten([
    for subnet_name in var.alb_config.subnets : [
      for subnet in var.subnets : subnet.id if subnet.tags.Name == subnet_name
    ]
  ])
  security_groups = toset([for security_group in var.alb_config.security_groups : var.security_groups[security_group].id])
  tags            = var.global_tags
}

resource "aws_lb_listener" "my_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.target_group.arn
    type             = "forward"
  }
  tags = var.global_tags
}

resource "aws_lb_target_group" "target_group" {
  name     = "target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc.id
  tags     = var.global_tags
}
