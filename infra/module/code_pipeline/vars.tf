# # logical handling var specific for codepipeline stages variable
# variable "code_pipeline_stages" {
#   type = list(object({
#     name = string
#     action = object({
#       name             = string
#       category         = string
#       owner            = string
#       provider         = string
#       version          = string
#       output_artifacts = list(string)
#       configuration    = map(string)
#     })
#   }))
#   default = [code_pipeline_config.stage]
# }

variable "code_pipeline_config" {

}

variable "s3_bucket" {

}

variable "iam_role" {

}

variable "global_config" {

}

variable "global_tags" {

}
