resource "aws_codepipeline" "codepipeline" {
  name     = var.code_pipeline_config.name
  role_arn = var.iam_role[var.code_pipeline_config.iam_role].arn # not defined yet

  artifact_store {
    location = var.s3_bucket[var.code_pipeline_config.artifact_store_bucket].bucket
    type     = "S3" # currently only S3 are supported, so chill...

    # encryption_key {
    #   id   = data.aws_kms_alias.s3kmskey.arn
    #   type = "KMS"
    # }
  }

  dynamic "stage" {
    for_each = var.code_pipeline_config.stage
    content {
      name = stage.key
      action {
        name             = stage.value.action.name
        category         = stage.value.action.category
        owner            = stage.value.action.owner
        provider         = stage.value.action.provider
        version          = stage.value.action.version
        output_artifacts = try(stage.value.action.output_artifacts, null)
        input_artifacts  = try(stage.value.action.input_artifacts, null)

        configuration = stage.value.action.configuration
      }
    }
  }
  tags = var.global_tags
}

resource "aws_codestarconnections_connection" "connection" {
  for_each      = var.code_pipeline_config.connection
  name          = each.key
  provider_type = each.value.type
  tags          = var.global_tags
}
