resource "aws_iam_role" "role" {
  for_each            = var.iam_role_config
  name                = each.key
  assume_role_policy  = each.value.assume_role_policy
  managed_policy_arns = [for policy in aws_iam_policy.policy : policy.arn]
  tags                = var.global_tags
}


resource "aws_iam_policy" "policy" {
  for_each    = var.iam_role_config
  name        = "${each.key}-policy"
  policy      = each.value.identity_role_policy
  description = each.value.description
  tags        = var.global_tags
}
