resource "aws_ecs_cluster" "ecs_cluster" {
  name = var.ecs_config.name
  tags = var.global_tags
}
