resource "aws_s3_bucket" "s3_bucket" {
  for_each = var.s3_config
  bucket   = each.key
  tags     = var.global_tags
}
