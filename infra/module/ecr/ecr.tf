resource "aws_ecr_repository" "ecr_repository" {
  name = var.ecr_config.repository_name
  tags = var.global_tags
}
