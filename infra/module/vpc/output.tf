output "vpc" {
  value = aws_vpc.vpc
}

output "subnets" {
  value = aws_subnet.subnet
}

output "eip" {
  value = aws_eip.eip
}

output "security_groups" {
  value = aws_security_group.sg
}