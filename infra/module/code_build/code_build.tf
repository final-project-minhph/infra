resource "aws_codebuild_project" "code_build_project" {
  name         = var.code_build_config.name
  description  = var.code_build_config.description
  service_role = var.iam_role.role[var.code_build_config.service_role].arn
  environment {
    compute_type = var.code_build_config.environment.compute_type
    image        = var.code_build_config.environment.image
    type         = var.code_build_config.environment.type
    # environment_variable {
    #   for_each = var.code_build_config.environment.environment_variable
    #   name  = each.key
    #   value = each.value
    # }
    privileged_mode = try(var.code_build_config.environment.privileged_mode, false)
  }
  build_timeout = var.code_build_config.build_timeout
  source {
    type     = var.code_build_config.source.type
    location = var.code_build_config.source.location
    # location = var.s3_bucket[var.code_build_config.source.location].bucket # to be fixed later
  }
  artifacts {
    type = var.code_build_config.artifacts.type
  }
  tags = var.global_tags
}
