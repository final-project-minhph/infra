# export VPC_NAME=${var.env}-minhph-vpc VPC_SUBNET_NAME= REGION=us-east-1 AVAILIBILITY_ZONE=us-east-1a

stages:
  - checkov
  - deploy

checkov:
  stage: checkov
  script:
    - |
      export BRANCH_NAME=$(echo $CI_COMMIT_REF_NAME | sed 's/\//-/g') &&
      echo "Branch Name: $BRANCH_NAME" &&
      ./checkov.sh $BRANCH_NAME
      
deploy:
  stage: deploy
  script:
    - export PATH=/opt/homebrew/bin:$PATH
    - terraform --version
    - |
      export BRANCH_NAME=$(echo $CI_COMMIT_REF_NAME | sed 's/\//-/g') &&
      echo "Branch Name: $BRANCH_NAME" &&
      ./run.sh $BRANCH_NAME
  only:
    - branches
  except:
    - tags
  when: manual
