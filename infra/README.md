### How the pipeline works:
- The push action triggers the checkov scripts
- The apply process needs to be done manually


### Each infra will have:
- Ecr module including container registry
- Ecs for running the applications
- An instance of mysql database on rds