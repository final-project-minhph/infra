# module "vpc" {
#   source = "../module/vpc"

#   global_config = local.global_config
#   global_tags       = local.global_tags
#   vpc_config    = local.configuration.vpc_config
# }


# module "alb" {
#   source = "../module/alb"

#   global_config   = local.global_config
#   global_tags     = local.global_tags
#   alb_config      = local.configuration.alb_config
#   vpc             = module.vpc.vpc
#   security_groups = module.vpc.security_groups
#   subnets         = module.vpc.subnets
#   depends_on      = [module.vpc]
# }


# module "ecs" {
#   source = "../module/ecs"

#   global_config = local.global_config
#   global_tags       = local.global_tags
#   ecs_config    = local.configuration.ecs_config
# }


# module "ecr" {
#   source = "../module/ecr"

#   global_config = local.global_config
#   global_tags       = local.global_tags
#   ecr_config    = local.configuration.ecr_config
# }


# module "database" {
#   source = "../module/database"

#   global_config = local.global_config
#   global_tags     = local.global_tags
#   rds_config = local.configuration.rds_config
#   subnets = module.vpc.subnets
# }


module "s3" {
  source = "../module/s3"

  global_config = local.global_config
  global_tags   = local.global_tags
  s3_config     = local.configuration.s3_config
}


module "iam_role" {
  source = "../module/iam"

  global_config   = local.global_config
  global_tags     = local.global_tags
  iam_role_config = local.configuration.iam_role_config
}


module "codepipeline" {
  source = "../module/code_pipeline"

  global_config        = local.global_config
  global_tags          = local.global_tags
  code_pipeline_config = local.configuration.code_pipeline_config
  iam_role = module.iam_role.role
  s3_bucket = module.s3.s3_bucket

  depends_on = [module.s3, module.iam_role]
}


# module "codebuild_project" {
#   source = "../module/code_build"

#   global_config     = local.global_config
#   global_tags       = local.global_tags
#   code_build_config = local.configuration.code_build_config
#   iam_role          = module.iam_role
#   s3_bucket         = module.s3.s3_bucket

#   depends_on = [module.s3, module.iam_role]
# }
