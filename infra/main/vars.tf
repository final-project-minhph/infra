module "vars" {
  source = "../vars"

  env = var.env
}


locals {
  configuration = module.vars.configuration
  global_config = module.vars.global_variables
  global_tags   = module.vars.tags
}

variable "prefix" {
  description = "Prefix for resources names"
  default     = "proj"
}

variable "env" {
  type        = string
  description = "The env to run"
}

variable "access_key" {
  description = "Access key of AWS resources"
  type        = string
}

variable "secret_key" {
  description = "Secret key of AWS resources"
  type        = string
}
